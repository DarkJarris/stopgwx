@ECHO OFF
REM --- remember to invoke from ELEVATED command prompt!
REM --- or start the batch with context menu "run as admin".
SETLOCAL

REM --- (as of 2016-03-11):
REM  KB3035583 - GWX Update installs Get Windows 10 app in Windows 8.1 and Windows 7 SP1
REM  KB3021917 - Update to Windows 7 SP1 for performance improvements
REM  KB3012973 - Upgrade to Windows 10 Pro
REM  KB3146449 - Internet Explorer Upgrade Nagware

REM --- no longer blocking:
REM  KB2952664 - Compatibility update for upgrading Windows 7
REM  KB2976978 - Compatibility update for Windows 8.1 and Windows 8
REM  KB3022345 - Telemetry [Replaced by KB3068708]
REM  KB3068708 - Update for customer experience and diagnostic telemetry

REM --- uninstall updates
echo uninstalling updates ...
start "title" /b /wait wusa.exe /kb:3021917 /uninstall /quiet /norestart
echo  - next
start "title" /b /wait wusa.exe /kb:3035583 /uninstall /quiet /norestart
echo  - next
start "title" /b /wait wusa.exe /kb:3146449 /uninstall /quiet /norestart
echo  - done.
timeout 10

REM --- hide updates
echo hiding updates ...
start "title" /b /wait cscript.exe "%~dp0HideWindowsUpdates.vbs" 3021917 3035583 3012973 3146449
echo  - done.

REM --- add registry options to not start GWX
echo Setting registry values as fallback ...
REG ADD HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\Gwx /f /v DisableGwx /t REG_SZ /d 1
REG ADD HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate\OSUpgrade /f /v ReservationsAllowed /t REG_SZ /d 0
echo  - done.

echo ... COMPLETED (please remember to REBOOT windows, now)
pause
REM --- EOF