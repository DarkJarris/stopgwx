# README #


This program will remove, and then hide (preventing from redownloading automatically), the following Windows Updates that preload Windows 10, and install the GWX Notification box (nagware).

KB3035583 - GWX Update installs Get Windows 10 app in Windows 8.1 and Windows 7 SP1  
KB3021917 - Update to Windows 7 SP1 for performance improvements  
KB3012973 - Upgrade to Windows 10 Pro  
KB3146449 - Internet Explorer "Upgrade Now" nagware


To Use simply run the StopGWX.bat batch file as administrator (Right click -> run as Administrator).


### General Info ###

These scripts were collated from a few sources around the web

* Version 1.0.0
* Collated by: DarkJarris
* Inspired by: Colin Bowern: http://serverfault.com/a/341318